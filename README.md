Alexis BEAUJET - Joël ILUNGA KATUMBA

#Passerelle FTP/REST

----------------------------

##1. Introduction
Ce projet fournit une API REST afin de pouvoir intéragir avec un serveur FTP via le protocol HTTP.

Résumé des fonctionalités opérationnelles :

 - Fichiers
      - GET
      - PUT/POST
      - DELETE
 - Répertoires
      - GET JSON/HTML
 - Identification par token

----------------------------

## 2. Architecture

#### Resources proposées

Trois resources sont accessibles via la paserellle: FileResource, DirectoryResource et LoginResource. Les deux premières resources agissent sur les fichiers, la dernière permet juste de s'identifier.


#### Identifiation

L'identifiaction se fait par jeton. Ce jeton sera à placer dans chaque requête à la passerelle.
`GET /login` génère et retourne un nouveau jeton (un entier, ex: 575645).
Un `POST /login` avec les informations suivantes termineront l'identification :

	POST /login
	username=admin
	password=superPass
	token=575645

Le code de retour indiquera si l'identification s'est correctement déroulée ou non (200 ou 401).

Le jeton a une durée de vie limitée mais est renouvelé à chaque requête l'utilisant.

#### Gestion de fichiers et répertoires

Les intérations avec le serveur FTP utilisent la bibliothèque FTPClient de la fondation Apache. (*org.apache.commons.net.FTPClient*)

La classe *FileRessource* se charge de la gestion des fichiers. 

La méthode *getFile* fait le travail de téléchargement d'un fichier : 
  Un client est créé, et une fois après avoir reçu un token de *LoginToken*, celui-ci se connecte.
  Dès lors, le transfert de fichier en byte peut commencer, le fichier de *Response* est remplit.
  Le transfert terminé, le client est déconnecté et la réponse est retournée.

Pour envoyer un fichier, la méthode *put* a été créée.
  Afin d'assurer le bon fonctionnement, dès le départ une vérification du chemin du fichier mis en paramètre est vérifiée.
  Le résultat du test est envoyée sous la forme *Response*.
  Si ce chemin a passé le test avec succès, un client ftp est alors créé et connecté avec son token.
  Si aucun problème n'est rencontré, le transfert binaire est démarré.
  Une fois de plus, une réponse négative ou positive est envoyé selon la réussite ou non du transfert.

La suppression de fichier se fait par la méthode *getDeleteFile* qui fonctionne comme les méthodes précédentes, c'est à dire qu'un client est créé et connecté, puis le fichier mentionnée dans le chemin mis en paramètre est supprimé.

#### Templates

Un **petit** moteur de template (classe *Template*) est inclut pour fournir des réponses formatées en HTML et conserver un code clair.

----------------------------

##3. Gestion des erreurs

Des classes d'exceptions ont été créées afin de prendre en charge les erreurs pouvant survenir durant l'utilisation de la passerelle.

---

Pour les fichiers : 

	public class FileNotFoundException extends WebApplicationException {
	    public FileNotFoundException( String filename) {
	        super(
	                Response
	                        .status( Status.NOT_FOUND )
	                        .entity( "File not found : " + filename )
	                        .build()
	        );
	    }
	}

Cette exception est lancée lorsque le filename d'un paramètre n'est pas valide.
Si le chemin précisé n'existe pas, une *Response* est alors retournée en précisant les raisons de cette erreur, ici le message indique que le fichier est introuvable.

Cette exception à du être faite afin de contrer les erreurs survenant des écritures du chemin.

---                    
Pour les répertoires :

Cette exception est dans la lignée de celle causée par les fichiers.

	public class DirectoryNotFoundException extends WebApplicationException {
	    public DirectoryNotFoundException( String pathname) {
	        super(
	                Response
	                        .status( Status.NOT_FOUND )
	                        .entity( "Directory not found : " + pathname)
	                        .build()
	        );
	    }
	}

                    -----------    -----------

  Pour le FTPClient (public class FTPClient) :

Dans la méthode testCredentials(LoginToken token), deux exceptions peuvent être lancées :

1) 

	catch (IOException e) {
	            throw new UnreachableServerException("127.0.0.1", 2121);
	}

Cette exception a été créé afin de survenir au problème de connexion au serveur.

Pour traité celle-ci : 

	public class UnreachableServerException extends WebApplicationException {
	    public UnreachableServerException(String hostname, int port) {
	        super(
	                Response
	                        .status( Status.NOT_FOUND )
	                        .entity( "KO : Cannot connect to host \"" + hostname +":"+port+"\"")
	                        .build()
	        );
	    }
	}

Il arrive parfois que la connexion au serveur ne se passe as comme prévu, si ça arrive, cette exception est alors appelé afin de retourné une *Response* dans laquel il est inscrit que la connexion à l'hôte avec le port indiqué n'est pas possible.

Cette exception a du être écrite pour les erreurs possibles de connexion.

2) throw new InvalidCredentialsException(token);

Cette exception arrive lorsque l'authentification ne c'est pas fait correctement.

Afin de survenir à celle-ci : 

	public class InvalidCredentialsException extends WebApplicationException {
	    public InvalidCredentialsException( LoginToken token) {
	        super(
	                Response
	                        .status( Status.UNAUTHORIZED )
	                        .entity( "KO : Invalid credentials for token : " + token.getValue() )
	                        .build()
	        );
	    }
	}

---                    
  Pour le token : 
  
Une exception a été créé pour les tokens invalide.

Pour les gérer : 

	public class InvalidTokenException extends WebApplicationException {
	    public InvalidTokenException( String token) {
	        super(
	                Response
	                        .status( Status.UNAUTHORIZED )
	                        .entity( "Invalid or expired login token " + token )
	                        .build()
	        );
	    }
	}
 
  Une *Response* est retournée précisant que le problème de la connexion vient du token écrit.

Cette exception a été faite afin de répondre aux erreurs liées au token délivré afin de se connecter, avec celle-ci l'utilisateur sait que l'erreur vient du token et peut alors voir si celui qui lui a été donné est encore valide ou pas.

---

## 4. Code samples

Utilisation des templates pour générer des pages formatées en conservant un code *propre*.

    public Response getDirectoryListHTML(String dir,String tokenStr) {
        LoginToken token = LoginService.getInstance().getToken(tokenStr);
        
        StringBuilder sb = new StringBuilder();
        for(FTPFile file : this.getDirectoryContent(dir, token)){
	        String resUrl = this.getResourceURL(dir, file, token);

            sb.append("<a href='" + resUrl + "' class='name'>"+file.getName()+"</a><br />\n");
        }

        Template tpl = new Template("dir-content");

        tpl.setSubsitute("CURRENT_PATH", (dir.equals("/") ? dir : "/" + dir));
        tpl.setSubsitute("DIR_CONTENTS", sb.toString());
        tpl.setSubsitute("LOGIN_TOKEN", token.getValue());

        return Response.status(200).entity(tpl.render()).build();
    }
   
   ---
   Suppression d'un fichier sur le FTP.
   
	    public Response deleteFile(String tokenStr, String filename){
	
	        FTPClient client = new FTPClient();
	        client.connectFromToken(LoginService.getInstance().getToken(tokenStr));
	
	        try {
	            if(client.deleteFile(filename)) {
	                client.disconnect();
	                return Response.status(204).build();
	            }
	            return Response.status(502).entity("FTP Error " + client.getReplyString()).build();
	        } catch (IOException e) {
	            return Response.status(502).build();
	        }
	    }

Utilisation de la prise en charge des exceptions par le serveur Web pour un affichage correct des erreurs :

    public boolean connectFromToken(LoginToken token) {

        if(token.isValid() == false)
            throw new InvalidTokenException(token.getValue());

        try {
            this.connect(InetAddress.getByName("127.0.0.1"), 2121);
            
            if(this.login(token.getUsername(), token.getPassword()) == false)
                throw new InvalidCredentialsException(token);
                
            return true;
        } catch (IOException e) {
            throw new UnreachableServerException("127.0.0.1", 2121);
        }

---

	public class InvalidCredentialsException extends WebApplicationException {
	    public InvalidCredentialsException( LoginToken token) {
	        super(
	            Response
	                .status( Status.UNAUTHORIZED )
	                .header( "Content-type", "text/html" )
	                .entity( "KO : Invalid credentials for token : " + token.getValue() )
	                .build()
	        );
	    }
	}
Upload des fichiers via POST:

    
    public Response putFile(String tokenStr, String path, InputStream received) throws IOException {
        Response.ResponseBuilder response = Response.status(Response.Status.OK);

        FTPClient client = new FTPClient();
        client.connectFromToken(LoginService.getInstance().getToken(tokenStr));
        if(client.setFileType(FTP.BINARY_FILE_TYPE)== false)
            return Response.status(502).entity("Cannot set transfer mode to binary.").build();

        try {
            client.storeFile(path, received);
            client.disconnect();
        } catch (IOException ex) {
            return response.status(502).build();
        }
        return response.status(200).entity("OK").build();
    }
