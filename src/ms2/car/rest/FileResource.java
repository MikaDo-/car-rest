package ms2.car.rest;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import javax.ws.rs.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.util.HashMap;

/**
 *  Works on files (possible to delete, download or upload) and produces HTML or a file
 */
@Path("/file")
public class FileResource {

    /**
     * Delete the file given in parameter
     * @param tokenStr the token received at the connection
     * @param filename the path of the file which will be delete
     * @return the result of the action
     */
    @DELETE
    @Path("{filename: .+}-{token: \\d+}")
    @Produces("text/plain")
    public Response deleteFile(@PathParam("token") String tokenStr, @PathParam("filename") String filename){

        FTPClient client = new FTPClient();
        client.connectFromToken(LoginService.getInstance().getToken(tokenStr));

        try {
            if(client.deleteFile(filename)) {
                client.disconnect();
                return Response.status(204).build();
            }
            return Response.status(502).entity("FTP Error " + client.getReplyCode() + "\n" + client.getReplyString()).build();
        } catch (IOException e) {
            return Response.status(502).build();
        }
    }

     /*** Download the file specifies in parameter ***/
    @GET
    @Path("{filename: .+}-{token: \\d+}")
    @Produces("application/octet-stream")
    public Response getFile(@PathParam("token") String tokenStr, @PathParam("filename") String filename){

        LoginToken token = LoginService.getInstance().getToken(tokenStr);

        FTPClient client = new FTPClient();
        client.connectFromToken(token);

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            if(client.retrieveFile("/"+filename, outputStream)) {

                Response response = Response
                        .ok()
                        .type("application/octet-stream")
                        .header("Content-Disposition", "attachment; filename=\"" + Paths.get(filename).getFileName() + "\"")
                        .entity(outputStream.toByteArray())
                        .build();

                client.disconnect();

                return response;
            }
            else
                return Response.status(502).entity("FTP Error " + client.getReplyCode() + "\n" + client.getReplyString()).build();
        } catch (IOException e) {
            return Response.status(502).build();
        }
    }

    /*** upload the file specified in parameter ***/
    @PUT
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED, MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.TEXT_PLAIN)
    @Path("{path:.+}-{token: \\d+}")
    public Response putFile(@PathParam("token") String tokenStr, @PathParam("path") String path, InputStream received) throws IOException {
        Response.ResponseBuilder response = Response.status(Response.Status.OK);

        FTPClient client = new FTPClient();
        client.connectFromToken(LoginService.getInstance().getToken(tokenStr));
        if(client.setFileType(FTP.BINARY_FILE_TYPE)== false)
            return Response.status(502).entity("Cannot set transfer mode to binary.").build();

        try {
            client.storeFile(path, received);
            client.disconnect();
        } catch (IOException ex) {
            return response.status(502).build();
        }
        return response.status(200).entity("OK").build();
    }

    /**
     * @return the upload form for browser upload
     */
    @GET
    @Path("uploadForm-{token: \\d+}")
    @Produces("text/html")
    public String getUploadForm(@FormParam("token") final String tokenStr){

        Template form = new Template("upload-form", new HashMap<String, String>(){{
            put("UPLOAD_PATH", "testFile");
            put("UPLOAD_TOKEN", tokenStr);
        }});

        return form.render();
    }
}
