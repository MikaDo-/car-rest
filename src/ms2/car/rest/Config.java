package ms2.car.rest;

import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.Console;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexis on 16/03/16.
 */
public class Config {
    private static Config instance = null;
    private HashMap<String, String> entries = null;

    public String get(String k){
        return this.entries.get(k);
    }

    private Config(){
        this.entries = new HashMap<>();

        FileReader fr = null;
        JSONTokener cc = null;
        JSONObject json = null;
        try {
            fr = new FileReader("config.json");
            cc = new JSONTokener(fr);
            json= new JSONObject(cc);
            JSONObject gw = json.getJSONObject("gateway");
            String gwAddr = gw.getString("domain-name");
            this.entries.put("gateway.domain-name", gwAddr);

            JSONObject ftp = json.getJSONObject("ftp-server");
            String ftpHost = ftp.getString("hostname");
            String ftpPort = ftp.getString("port");
            this.entries.put("ftp-server.host", ftpHost);
            this.entries.put("ftp-server.port", ftpPort);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static Config getInstance() {
        if (Config.instance == null)
            Config.instance = new Config();
        return Config.instance;
    }
}
