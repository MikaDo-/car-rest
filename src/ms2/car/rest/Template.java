package ms2.car.rest;

import org.apache.cxf.helpers.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Models of the HTML page
 */
public class Template {

    private String template;
    private Map<String, String> subsitutes;

    public Template() {

        this.template = new String();

        this.subsitutes = new HashMap<String, String>();

    }

    public Template(String template) {

        this.template = template;

        this.subsitutes = new HashMap<String, String>();

    }

    public Template(String template, Map<String, String> subsitutes) {

        this.template = template;

        this.subsitutes = subsitutes;

    }

    /**
     * Renders the final HTML output.
     * @return HTML
     */
    public String render(){
        File tpl = new File("./html/"+template+".html");

        String content = null;

        try {
            content = IOUtils.toString(new FileInputStream(tpl), null);
        } catch (FileNotFoundException e) {
            System.out.println("Missing template : " + this.template);
        } catch (IOException e) {
            System.out.println("Unexpected template error : " + this.template);
        }

        for (Map.Entry<String, String> entry : subsitutes.entrySet())
            content = content.replaceAll(entry.getKey(), entry.getValue());

        return content;

    }

    public void setTplName(String name){
        this.template = name;
    }

    /**
     * Sets values for template variables
     * @param values
     */
    public void setSubsitutes(Map<String, String> values){
        this.subsitutes = values;
    }

    /**
     * Sets  the value for a template variable
     * @param k Variable name
     * @param v Value
     */
    public void setSubsitute(String k, String v){
        this.subsitutes.put(k, v);
    }

    public String getSubsituteValue(String k){
        return this.subsitutes.get(k);
    }

}
