/**
 * Contains all the classes useful at the actions of the RestGateway
 */
package ms2.car.rest;

import ms2.car.restFramework.exceptions.FileNotFoundException;
import org.apache.commons.net.ftp.FTPFile;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.file.Paths;


/**
 * Provides GET actions on directories, outputting HTML or JSON.
 */
@Path("/dir")
public class DirectoryResource{

    private FTPFile[] getDirectoryContent(String dir, LoginToken token) throws IOException {

        FTPClient client = new FTPClient();
        client.connectFromToken(token);

        if(client.changeWorkingDirectory("/"+dir) == false)
            throw new FileNotFoundException(dir);

        FTPFile[] contents = null;
        contents = client.mlistDir();

        return contents;
    }

    /**
     * List the root directory
     * @param tokenStr the token received at the connection
     * @return the JSON listing of the directory
     */
    @GET
    @Path("/-{token: \\d+}")
    @Produces( { MediaType.APPLICATION_JSON  } )
    public Response getRootDirectoryListJSON(@PathParam("token") String tokenStr) {
        return this.getDirectoryListJSON("/", tokenStr);
    }

    /**
     * List the root directory
     * @return HTML output
     */
    @GET
    @Path("/-{token: \\d+}")
    @Produces( { MediaType.TEXT_HTML  } )
    public Response getRootDirectoryListHTML(@PathParam("token") String tokenStr) {
        return this.getDirectoryListHTML("/", tokenStr);
    }

    /**
     * List the directory given
     * @param dir the path of the directory which will be list
     * @param tokenStr the token received at the connection
     * @return the JSON listing of the directory
     */
    @GET
    @Path("/{dir: .+}-{token: \\d+}")
    @Produces( { MediaType.APPLICATION_JSON  } )
    public Response getDirectoryListJSON(@PathParam("dir") String dir, @PathParam("token") String tokenStr) {
        LoginToken token = LoginService.getInstance().getToken(tokenStr);
        try {
            return Response.status(200)
                    .entity(this.getDirectoryContent(dir, token))
                    .header("Content-type", "application/json")
                    .build();
        } catch (IOException e) {
            return Response.status(500)
                    .entity("Unexpected I/O error when exchanging data with the FTP server.")
                    .header("Content-type", "text/plain")
                    .build();
        }
    }

    private String getResourceURL(String dir, FTPFile file, LoginToken token){

        Config cfg = Config.getInstance();
        String gwDn = cfg.get("gateway.domain-name");
        return "http://"+gwDn+":8080/rest/api/" + (file.isDirectory() ? "dir" : "file") + ( (dir.isEmpty() || dir.equals("/")) ? "" : "/") + dir + "/" + file.getName() + "-" + token.getValue();
    }

    /**
     * List the directory given
     * @return HTML output
     */
    @GET
    @Path("/{dir: .+}-{token: \\d+}")
    @Produces( { MediaType.TEXT_HTML  } )
    public Response getDirectoryListHTML(@PathParam("dir") String dir, @PathParam("token") String tokenStr) {

        LoginToken token = LoginService.getInstance().getToken(tokenStr);
        FTPFile[] contents;
        try {
            contents = this.getDirectoryContent(dir, token);
        } catch (IOException e) {
            return Response.status(500)
                    .entity("Unexpected I/O error when exchanging data with the FTP server.")
                    .header("Content-type", "text/plain")
                    .build();
        }
        StringBuilder sb = new StringBuilder();

        String resUrl;
        if(dir.isEmpty() == false && dir.equals("/") == false) {
            java.nio.file.Path pt = Paths.get("/" + dir).getParent();
            String path = pt.toString();
            Config cfg = Config.getInstance();
            String gwDn = cfg.get("gateway.domain-name");
            resUrl = "http://"+gwDn+":8080/rest/api/dir" + ((path.isEmpty() || path.equals("/")) ? "" : "/") + path + "-" + token.getValue();
            sb.append("<tr class='dir'>\n" +
                    //"\t<td><a href='" + resUrl + "' class='name'>..</a></td>\n" +
                    "\t<td></td>\n" +
                    "\t<td></td>\n" +
                    "</tr>\n");
        }
        for(FTPFile file : contents){
            resUrl = this.getResourceURL(dir, file, token);

            sb.append("<tr class='"+ (file.isDirectory() ? "dir" : "file") + "'>\n" +
                          "\t<td><a href='" + resUrl + "' class='name'>"+file.getName()+"</a></td>\n" +
                          "\t<td><a href='" + resUrl + "'>"+ file.getSize() +"o</a></td>\n" +
                          "\t<td><a href='" + resUrl + "'>" + file.getTimestamp().getTime().toString() + "</a></td>\n" +
                      "</tr>\n");
        }

        Template tpl = new Template("dir-content");

        tpl.setSubsitute("CURRENT_PATH", (dir.equals("/") ? dir : "/" + dir));
        tpl.setSubsitute("DIR_CONTENTS", sb.toString());
        tpl.setSubsitute("LOGIN_TOKEN", token.getValue());

        return Response.status(200).entity(tpl.render()).build();
    }
}