package ms2.car.rest;

import java.util.Date;

/**
 * Modelize an user which is compulsory to authentication.
 * This class uses a token which is given by the LoginService class.
 */
public class LoginToken {

    private String username;
    private String password;
    private String value;
    private Date expiresOn;
    private boolean isValid;

    public LoginToken() {
        isValid = false;
        username = new String();
        value = new String();
        expiresOn = new Date();
    }

    public LoginToken(String value) {
        this.value = value;
        isValid = false;
        username = new String();
        expiresOn = new Date();
    }

    public LoginToken(String value, String user, String password, Date expiresOn) {
        isValid = false;
        this.value = value;
        this.username = user;
        this.password = password;
        this.expiresOn = expiresOn;
    }

    public void renew(){
        this.expiresOn = new Date();
        this.expiresOn.setTime(new Date().getTime() + 5*60*1000);
    }

    @Override
    public String toString(){
        return this.getValue();
    }

    public String getValue() {
        return value;
    }

    public boolean isExpired(){
        return this.expiresOn.before(new Date());
    }

    public boolean isValid(){
        return this.isValid && !this.isExpired();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(Date expiresOn) {
        this.expiresOn = expiresOn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }
}
