package ms2.car.rest;

import javax.ws.rs.*;

/**
 * Try to establish a connection with token and the result is visible in HTML.
 */
@Path("/login")
public class LoginResource {

    @POST
    @Produces("text/html")
    public String postLogin(@FormParam("token") String tokenStr, @FormParam("username") String username, @FormParam("password") String password){
        LoginToken token = LoginService.getInstance().getToken(tokenStr);

        token.setValid(false);
        token.setUsername(username);
        token.setPassword(password);

        if(new FTPClient().testCredentials(token)){
            token.renew();
            token.setValid(true);
            return "OK - token : " + token.getValue();
        }else
            return "KO-login : IMPOSSIBRU !";
    }

    /**
     * Issues a new token
     */
    @GET
    @Produces("text/html")
    public String getLogin(){
        return String.valueOf(LoginService.getInstance().generateNewToken());
    }

    /**
     * Issues a new token
     */
    @GET
    @Path("/form")
    @Produces("text/html")
    public String getLoginForm(){

        Template tpl = new Template("login-form");
        tpl.setSubsitute("LOGIN_TOKEN", LoginService.getInstance().generateNewToken());

        return tpl.render();
    }

}