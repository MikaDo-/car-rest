package ms2.car.rest;

import ms2.car.restFramework.exceptions.InvalidCredentialsException;
import ms2.car.restFramework.exceptions.InvalidTokenException;
import ms2.car.restFramework.exceptions.UnreachableServerException;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Allows to configure the FTPClient
 */
public class FTPClient extends org.apache.commons.net.ftp.FTPClient{

    /**
     * Tests whether the given credentials are valid or not.
     * @param token
     * @return boolean
     */
    public boolean testCredentials(LoginToken token){
        Config cfg = Config.getInstance();
        String ftpHost = cfg.get("ftp-server.host");
        String ftpPort = cfg.get("ftp-server.port");

        try {
            this.connect(InetAddress.getByName(ftpHost), Integer.decode(ftpPort));

            if(this.login(token.getUsername(), token.getPassword()) == false)
                throw new InvalidCredentialsException(token);

            this.disconnect();

            return true;
        } catch (IOException e) {
            throw new UnreachableServerException(ftpHost, Integer.decode(ftpPort));
        }
    }

    /**
     * Establishes a connection with the FTP sever using a LoginToken as
     * credentials.
     * @param token
     * @return connection success.
     */
    public boolean connectFromToken(LoginToken token) {

        if(token.isValid() == false)
            throw new InvalidTokenException(token.getValue());

        Config cfg = Config.getInstance();
        String ftpHost = cfg.get("ftp-server.host");
        String ftpPort = cfg.get("ftp-server.port");
        try {
            this.connect(InetAddress.getByName(ftpHost), Integer.decode(ftpPort));

            if(this.login(token.getUsername(), token.getPassword()) == false)
                throw new InvalidCredentialsException(token);

            return true;

        } catch (IOException e) {
            throw new UnreachableServerException(ftpHost, Integer.decode(ftpPort));
        }

    }

    @Override
    public void disconnect() {

        try {
            super.disconnect();
        } catch (IOException e) {
        }

    }

}
