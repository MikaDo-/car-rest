package ms2.car.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * SINGLETON - Provides and check the validity of login tokens.
 */
public class LoginService {

    // List of all the current sessions.
    private Map<String, LoginToken> sessions;

    private static LoginService instance = null;

    public LoginService() {
        this.sessions = new HashMap<String, LoginToken>();
    }

    public static LoginService getInstance(){
        if (LoginService.instance == null)
            return LoginService.instance = new LoginService();
        return LoginService.instance;
    }

    /**
     * Verify if the token put in parameter does not exceed a deadline
     */
    public boolean isTokenValid(String token){
        return this.sessions.containsKey(token) && sessions.get(token).isValid();
    }

    /**
     *
     * @return Token value as a String.
     */
    public String generateNewToken(){

        String tokenStr = Integer.toString(new Random().nextInt((2000000 - 1000000) + 1) + 100000);
        sessions.put(tokenStr, new LoginToken(tokenStr));

        return tokenStr;
    }

    /**
     * Returns a LoginToken by its identifier (string value)
     * @param token String
     * @return LoginToken or null if not registered
     */
    public LoginToken getToken(String token){
        /*
         // For testing purpose only !

        Date date = new Date();
        date.setTime(new Date().getTime() + 5*60*1000);

        LoginToken t = new LoginToken("010101", "admin", "admin", date);
        t.setValid(true);

        return t;
*/
        Map<String, LoginToken> sessions = LoginService.getInstance().sessions;
        if(sessions.containsKey(token) == false)
            throw new InvalidTokenException(token);
        return sessions.get(token);

    }
}
