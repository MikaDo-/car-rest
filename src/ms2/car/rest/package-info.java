/**
 * Par Alexis Beaujet & Clément Bellart M1 - 2015
 *
 * Ce projet fournit une API REST afin de pouvoire intéragir avec un serveur FTP via le protocol HTTP.
 *
 * Résumé des fonctionalités opérationnelles :
 *
 * - Fichiers
 *   - GET
 *   - PUT/POST
 *   - DELETE
 * - Répertoires
 *   - GET JSON/HTML
 * - Identification par token
 */
package ms2.car.rest;