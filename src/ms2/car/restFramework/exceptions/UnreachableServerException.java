package ms2.car.restFramework.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class UnreachableServerException extends WebApplicationException {
    public UnreachableServerException(String hostname, int port) {
        super(
                Response
                        .status( Status.NOT_FOUND )
                        .entity( "KO : Cannot connect to host \"" + hostname +":"+port+"\"")
                        .build()
        );
    }
}