
package ms2.car.restFramework.exceptions;

import ms2.car.rest.LoginToken;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class InvalidCredentialsException extends WebApplicationException {
    public InvalidCredentialsException( LoginToken token) {
        super(
            Response
                .status( Status.UNAUTHORIZED )
                .header( "Content-type", "text/html" )
                .entity( "KO : Invalid credentials for token : " + token.getValue() )
                .build()
        );
    }
}