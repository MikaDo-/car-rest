package ms2.car.restFramework.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class InvalidTokenException extends WebApplicationException {
    public InvalidTokenException( String token) {
        super(
                Response
                        .status( Status.UNAUTHORIZED )
                        .header( "Content-type", "text/html" )
                        .entity( "KO : Invalid or expired login token " + token )
                        .build()
        );
    }
}
