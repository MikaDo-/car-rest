package ms2.car.restFramework.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class FileNotFoundException extends WebApplicationException {
    public FileNotFoundException( String filename) {
        super(
            Response
                .status( Status.NOT_FOUND )
                .header("Content-type", "text/html")
                .entity(filename + " : No such file or directory")
                .build()
        );
    }
}