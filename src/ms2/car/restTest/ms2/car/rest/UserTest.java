package ms2.car.rest;

import junit.framework.TestCase;
import org.junit.Test;

public class UserTest extends TestCase{

    private User connection = new User("te","st");

    @Test
    public void testSetUsername() throws Exception {

        System.out.println("Test le setUsername de User");

        this.connection.setUsername("test");

        assertEquals("test", this.connection.getUsername());

    }

    @Test
    public void testSetPassword() throws Exception {

        System.out.println("Test le setPassword de User");

        this.connection.setPassword("test");

        assertEquals("test", this.connection.getPassword());

    }

}
