package ms2.car.rest;

import junit.framework.TestCase;
import org.junit.Test;

public class LoginTokenTest extends TestCase {

    LoginToken loginToken = new LoginToken();

    /*public void testRenew() throws Exception {

    }*/

    @Test
    public void testGetValue() throws Exception {

        System.out.println("Test le getValue de LoginToken");

        assertEquals("",loginToken.getValue());

    }

    @Test
    public void testIsExpired() throws Exception {

        System.out.println("Test le isExpired de LoginToken");

        assertEquals(true, loginToken.isExpired());

    }

    @Test
    public void testSetUsername() throws Exception {

        System.out.println("Test le setUsername de LoginToken");

        loginToken.setUsername("test");

        assertEquals("test", loginToken.getUsername());

    }

    /*public void testSetExpiresOn() throws Exception {

        Date expire = new Date();

        expire.setTime(new Date().getTime() + 5*60*1000);

        loginToken.setExpiresOn(expire);

        assertEquals(true, loginToken.getExpiresOn());
    }*/

    @Test
    public void testSetPassword() throws Exception {

        System.out.println("Test le setPassword de LoginToken");

        loginToken.setPassword("pass");

        assertEquals("pass", loginToken.getPassword());

    }

    @Test
    public void testSetValid() throws Exception {

        loginToken.setValid(false);

        assertEquals(false, loginToken.isValid());

    }

}